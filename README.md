# Real StarField for Godot
Uses the [HYG Database](https://github.com/astronexus/HYG-Database) to render over 100,000 stars for 3D games.

![Rendered stars](https://gitlab.com/croxis/starfield/-/raw/trunk/addons/star_field/screenshots/stars.gif)

## Versions
Built on Godot 4.0
VR should work but is currently untested.

## How it works
Activate in the editor plugins/
This is a plugin that creates a new node type called Star Field. The stars are a MultiMeshInstance and are set by shaders to always render in the background, behind other objects in the scene.
Stars brightness is calculated based on the star's absolute magnitude and camera distance.
Data is stored in a csv file. It will add a few seconds to load times.
1 Godot unit = 1 parsec. Use the camera_path parameter to keep the starfield centered on the camera.

## Parameters
Sun Brightness: Approximately how many pixels our Sun is when viewed 1 unit away. Unless I understood the math wrong.
A: Lower values make brighter stars brighter.
Celestial Coords: Shifts the origin of the starfield. 0, 0, 0 is our sun. Units are in parsecs.
Camera Path: The nodepath to the rendering camera. This keeps the star scene centered on the camera, but allows the camera to rotate without the entire scene.

## Demo
An example scene is in addons/star_field/Example.tscn

## Screenshots
All stars in the HYG database
![All stars](https://gitlab.com/croxis/starfield/-/raw/trunk/addons/star_field/screenshots/all_stars.2021.12.22.png)

